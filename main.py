import numpy as np

matrix = np.loadtxt(fname='/config/matrix.txt', delimiter=',', unpack=True)

"""
# Test 5x5 - 2427.0
matrix = np.array([[131, 673, 234, 103, 18],
                   [201, 96, 342, 965, 150],
                   [630, 803, 746, 422, 111],
                   [537, 699, 497, 121, 956],
                   [805, 732, 524, 37, 331]])
"""

sum_matrix = np.full((matrix.shape[0], matrix.shape[1]), np.inf)

queue = list()

# start from cell (0, 0)
row = 0
col = 0

# initialize sum of matrix
queue = [(row, col)]
sum_matrix[(row, col)] = matrix[(row, col)]

# Start recursive lookup
while len(queue) > 0:
    # LIFO - Last In First Out
    cur_cell = queue.pop()
    col = cur_cell[1]
    row = cur_cell[0]

    if sum_matrix[cur_cell] < sum_matrix[(-1, -1)]:
        # move right
        next_col = col + 1
        if next_col < matrix.shape[1]:
            next_col_sum = sum_matrix[cur_cell] + matrix[(row, next_col)]
            if next_col_sum < sum_matrix[(row, next_col)]:
                sum_matrix[(row, next_col)] = sum_matrix[cur_cell] + matrix[(row, next_col)]
                # append cell on right to the queue
                queue.append((row, next_col))

        # move down
        next_row = row + 1
        if next_row < matrix.shape[0]:
            next_row_sum = sum_matrix[cur_cell] + matrix[(next_row, col)]
            if next_row_sum < sum_matrix[(next_row, col)]:
                sum_matrix[(next_row, col)] = sum_matrix[cur_cell] + matrix[(next_row, col)]
                # append cell below to the queue
                queue.append((next_row, col))

print('Shortest path sum is: ', sum_matrix[-1, -1])
